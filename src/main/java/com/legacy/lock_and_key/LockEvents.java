package com.legacy.lock_and_key;

import java.util.ArrayList;
import java.util.List;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.LockCode;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.BaseContainerBlockEntity;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.entity.Hopper;
import net.minecraft.world.level.block.entity.HopperBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.ChestType;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.level.BlockEvent;
import net.minecraftforge.event.level.ExplosionEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class LockEvents
{
	@SubscribeEvent
	public void onRightClickBlock(PlayerInteractEvent.RightClickBlock event)
	{
		Level level = event.getLevel();
		BlockState block = level.getBlockState(event.getPos());

		if (!level.isClientSide && block.hasBlockEntity() && level.getBlockEntity(event.getPos())instanceof BaseContainerBlockEntity lockedTile && event.getEntity().getMainHandItem().getItem() == LockAndKeyMod.ModRegistry.KEY.get() && !isNeighborLocked(lockedTile, event.getEntity()))
		{
			if (lockedTile.lockKey == LockCode.NO_LOCK && !event.getEntity().isShiftKeyDown())
			{
				if (event.getEntity().getMainHandItem().getItem() == LockAndKeyMod.ModRegistry.KEY.get() && !event.getEntity().getMainHandItem().hasCustomHoverName())
				{
					event.getEntity().displayClientMessage(Component.translatable("gui.lock_and_key.no_name"), true);
					level.playSound(null, event.getPos(), SoundEvents.BARREL_CLOSE, SoundSource.BLOCKS, 1.0F, 1.5F);
				}
				else
				{
					lockedTile.lockKey = new LockCode(event.getEntity().getMainHandItem().getHoverName().getString());
					event.getEntity().displayClientMessage(Component.translatable("gui.lock_and_key.locked", lockedTile.getDisplayName()), true);
					level.playSound(null, event.getPos(), SoundEvents.CHEST_LOCKED, SoundSource.BLOCKS, 1.0F, 0.5F);
				}

				event.setCanceled(true);
			}
			else if (lockedTile.lockKey != LockCode.NO_LOCK && event.getEntity().isShiftKeyDown() && canUnlockTile(lockedTile.lockKey, event.getEntity().getMainHandItem()))
			{
				event.getEntity().swing(InteractionHand.MAIN_HAND, true);
				lockedTile.lockKey = LockCode.NO_LOCK;
				event.getEntity().displayClientMessage(Component.translatable("gui.lock_and_key.unlocked", lockedTile.getDisplayName()), true);
				level.playSound(null, event.getPos(), SoundEvents.CHEST_LOCKED, SoundSource.BLOCKS, 1.0F, 1.5F);
			}
		}
	}

	@SubscribeEvent
	public void onBlockBreak(BlockEvent.BreakEvent event)
	{
		Level world = event.getPlayer().level;
		BlockState block = event.getState();

		if (!world.isClientSide && block.hasBlockEntity() && world.getBlockEntity(event.getPos())instanceof BaseContainerBlockEntity lockedTile && !event.getPlayer().isCreative())
		{
			if (!canOpenChest(event.getPlayer(), lockedTile.lockKey, null) || isNeighborLocked(lockedTile, event.getPlayer()))
				event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public void onExplosionDetonate(ExplosionEvent.Detonate event)
	{
		List<BlockPos> blocks = new ArrayList<BlockPos>(event.getAffectedBlocks());
		Level level = event.getLevel();

		blocks.forEach((position) ->
		{
			if (!level.isClientSide && level.getBlockEntity(position)instanceof BaseContainerBlockEntity tile && (tile.lockKey != LockCode.NO_LOCK || getNeighborChest(tile) != null && getNeighborChest(tile).lockKey != LockCode.NO_LOCK))
				event.getAffectedBlocks().remove(position);
		});
	}

	/**
	 * Only play effects if the containerName if it exists
	 */
	public static boolean canOpenChest(Player player, LockCode code, Component containerName)
	{
		if (player.isCreative())
			return true;

		boolean canUnlock = canUnlockTile(code, player.getMainHandItem());

		if (!player.isSpectator() && !canUnlock && player.getMainHandItem().getItem() == LockAndKeyMod.ModRegistry.KEY.get())
		{
			if (containerName != null)
			{

				player.displayClientMessage(Component.translatable("gui.lock_and_key.wrong_key", containerName), true);
				player.playNotifySound(SoundEvents.CHEST_LOCKED, SoundSource.BLOCKS, 1.0F, 1.0F);
			}

			return false;
		}
		else if (!player.isSpectator() && !canUnlock)
		{
			if (containerName != null)
			{
				player.displayClientMessage(Component.translatable("container.isLocked", containerName), true);
				player.playNotifySound(SoundEvents.CHEST_LOCKED, SoundSource.BLOCKS, 1.0F, 1.0F);
			}

			return false;
		}

		return true;
	}

	private static boolean canUnlockTile(LockCode lockCode, ItemStack stack)
	{
		/*if it has no lock, or the stack is not empty, it has a name, of which is the lock name, and it is also a key*/
		return lockCode.key.isEmpty() || !stack.isEmpty() && stack.hasCustomHoverName() && lockCode.key.equals(stack.getHoverName().getString()) && stack.getItem() == LockAndKeyMod.ModRegistry.KEY.get();
	}

	private static boolean isNeighborLocked(BaseContainerBlockEntity tile, Player player)
	{
		ChestBlockEntity neighbor = getNeighborChest(tile);

		if (neighbor != null)
			return !canOpenChest(player, neighbor.lockKey, null);

		return false;
	}

	private static ChestBlockEntity getNeighborChest(BaseContainerBlockEntity tile)
	{
		if (tile instanceof ChestBlockEntity && tile.getBlockState().hasProperty(ChestBlock.TYPE) && tile.getBlockState().getValue(ChestBlock.TYPE) != ChestType.SINGLE && tile.hasLevel() && tile.getLevel().getBlockEntity(tile.getBlockPos().relative(ChestBlock.getConnectedDirection(tile.getBlockState())))instanceof ChestBlockEntity neighbor)
			return neighbor;

		return null;
	}

	public static void pullItemsFromHopper(Level level, Hopper hopper, CallbackInfoReturnable<Boolean> callback)
	{
		BlockEntity be = level.getBlockEntity(BlockPos.containing(hopper.getLevelX(), hopper.getLevelY() + 1, hopper.getLevelZ()));

		if (be instanceof BaseContainerBlockEntity locked)
		{
			if (hopper instanceof HopperBlockEntity currentHopper)
			{
				if (!locked.lockKey.key.equals(currentHopper.lockKey.key))
				{
					callback.setReturnValue(false);
					return;
				}
			}
			else if (!locked.lockKey.key.isBlank())
			{
				callback.setReturnValue(false);
				return;
			}

			ChestBlockEntity neighbor = getNeighborChest(locked);

			if (neighbor != null && !neighbor.lockKey.key.isBlank())
			{
				callback.setReturnValue(false);
				return;
			}
		}
	}
}
