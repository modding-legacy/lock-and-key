package com.legacy.lock_and_key;

import com.legacy.lock_and_key.item.KeyItem;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Lazy;
import net.minecraftforge.event.CreativeModeTabEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegisterEvent;

@Mod(LockAndKeyMod.MODID)
public class LockAndKeyMod
{
	public static final String NAME = "Lock and Key";
	public static final String MODID = "lock_and_key";

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(LockAndKeyMod.MODID, name);
	}

	public static String find(String name)
	{
		return LockAndKeyMod.MODID + ":" + name;
	}

	public LockAndKeyMod()
	{
		MinecraftForge.EVENT_BUS.register(new LockEvents());
		
		FMLJavaModLoadingContext.get().getModEventBus().addListener(ModRegistry::registerCreativeTabs);

	}

	@EventBusSubscriber(modid = LockAndKeyMod.MODID, bus = Bus.MOD)
	public static class ModRegistry
	{
		public static final Lazy<Item> KEY = Lazy.of(() -> new KeyItem(new Item.Properties().stacksTo(1)));

		@SubscribeEvent
		public static void onRegisterItems(RegisterEvent event)
		{
			if (event.getRegistryKey().equals(ForgeRegistries.Keys.ITEMS))
				event.register(ForgeRegistries.Keys.ITEMS, locate("key"), KEY);
		}
		
		public static void registerCreativeTabs(CreativeModeTabEvent.BuildContents event)
		{
			if (event.getTab() == CreativeModeTabs.TOOLS_AND_UTILITIES)
				event.accept(KEY.get());
		}
	}
}
