package com.legacy.lock_and_key.item;

import net.minecraft.world.item.Item;
import net.minecraft.world.item.Vanishable;

public class KeyItem extends Item implements Vanishable
{
	public KeyItem(Properties properties)
	{
		super(properties);
	}
}
