package com.legacy.lock_and_key.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.lock_and_key.LockEvents;

import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.entity.BaseContainerBlockEntity;
import net.minecraft.network.chat.Component;
import net.minecraft.world.LockCode;

@Mixin(BaseContainerBlockEntity.class)
public class BaseContainerBlockEntityMixin
{
	@Inject(at = @At("HEAD"), method = "canUnlock(Lnet/minecraft/world/entity/player/Player;Lnet/minecraft/world/LockCode;Lnet/minecraft/network/chat/Component;)Z", cancellable = true)
	private static void canUnlock(Player playerIn, LockCode lockCodeIn, Component nameIn, CallbackInfoReturnable<Boolean> callback)
	{
		callback.setReturnValue(LockEvents.canOpenChest(playerIn, lockCodeIn, nameIn));
	}
}
