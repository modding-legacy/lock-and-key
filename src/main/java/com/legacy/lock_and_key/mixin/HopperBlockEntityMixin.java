package com.legacy.lock_and_key.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.lock_and_key.LockEvents;

import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.Hopper;
import net.minecraft.world.level.block.entity.HopperBlockEntity;

@Mixin(HopperBlockEntity.class)
public class HopperBlockEntityMixin
{
	/*@Inject(at = @At(value = "INVOKE", target = "net/minecraft/core/Direction.DOWN:Lnet/minecraft/core/Direction;"), method = "suckInItems", cancellable = true)
	public static void suckInItems(Level p_155553_, Hopper p_155554_, CallbackInfoReturnable<Boolean> callback)
	{
		System.out.println();
	}*/
	
	/*@Inject(at = @At("HEAD"), method = "tryTakeInItemFromSlot", cancellable = true)
	private static void suckInItems(Hopper p_59355_, Container p_59356_, int p_59357_, Direction p_59358_, CallbackInfoReturnable<Boolean> callback)
	{
		LockEvents.pullItemsFromHopper(p_59355_, p_59356_, p_59357_, callback);
	}*/
	
	@Inject(at = @At("HEAD"), method = "suckInItems", cancellable = true)
	private static void suckInItems(Level p_155553_, Hopper p_155554_, CallbackInfoReturnable<Boolean> callback)
	{
		LockEvents.pullItemsFromHopper(p_155553_, p_155554_, callback);
	}
}
